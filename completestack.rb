#!/usr/bin/ruby
require 'aws-sdk'
require 'base64'

ec2 = Aws::EC2::Resource.new(region: 'us-east-2')
elb = Aws::ElasticLoadBalancing::Client.new(region: 'us-east-2')
vpc = ec2.create_vpc({ cidr_block: '10.200.0.0/16' })
rds = Aws::RDS::Client.new(region: 'us-east-2')

# So we get a public DNS
vpc.modify_attribute({
  enable_dns_support: { value: true }
})
vpc.modify_attribute({
  enable_dns_hostnames: { value: true }
})

# Name our VPC
vpc.create_tags({ tags: [{ key: 'Name', value: 'Gc-Trial' }]})
puts vpc.vpc_id + '-->VPC For GC-LAB' 

# create IGW
igw = ec2.create_internet_gateway

# Tagging & attaching IGW to VPC

igw.create_tags({ tags: [{ key: 'Name', value: 'Gc-TrialIGW' }]})
igw.attach_to_vpc(vpc_id: vpc.vpc_id)
puts igw.id + '-->Internet gateway for GC-LAB'

# Creating Public & Private Subneti

subnet1 = ec2.create_subnet({
  vpc_id: vpc.vpc_id,
  cidr_block: '10.200.10.0/24',
  availability_zone: 'us-east-2a'
})

subnet1.create_tags({ tags: [{ key: 'Name', value: 'Public-1-Gc-Trial' }]})
puts subnet1.id + '-->First Public Subnet for Web servers'

subnet2 = ec2.create_subnet({
  vpc_id: vpc.vpc_id,
  cidr_block: '10.200.11.0/24',
  availability_zone: 'us-east-2b'
})

subnet2.create_tags({ tags: [{ key: 'Name', value: 'Public-2-Gc-Trial' }]})
puts subnet2.id + '-->Second Public subnet for Web Servers'

subnet3 = ec2.create_subnet({
  vpc_id: vpc.vpc_id,
  cidr_block: '10.200.13.0/24',
  availability_zone: 'us-east-2c'
})

subnet3.create_tags({ tags: [{ key: 'Name', value: 'Private-1-Gc-Trial' }]})
puts subnet3.id + '-->First Private subnet for RDS'

subnet4 = ec2.create_subnet({
  vpc_id: vpc.vpc_id,
  cidr_block: '10.200.14.0/24',
  availability_zone: 'us-east-2a'
})

subnet4.create_tags({ tags: [{ key: 'Name', value: 'Private-2-Gc-Trial' }]})
puts subnet4.id + '-->Second private subnet for RDS'

# Create Route table & attach it to the public subnet.

table = ec2.create_route_table({
    vpc_id: vpc.vpc_id 
  })
table.create_tags({ tags: [{ key: 'Name', value: 'Gc-TrialRouteTable' }]})

table.create_route({
  destination_cidr_block: '0.0.0.0/0',
  gateway_id: igw.id
})

table.associate_with_subnet({
  subnet_id: subnet1.id,
})

table.associate_with_subnet({
  subnet_id: subnet2.id,
})
puts table.id + '--> Route table for GC-LAB'

# Security group for ELB

sgelb = ec2.create_security_group({
    group_name: 'Gc-Trial-ELB-SG-Group',
    description: 'Security group for Gc-Trial-ELB-SG',
    vpc_id: vpc.vpc_id
  })

sgelb.authorize_ingress({
  ip_permissions: [
    {
      from_port: 80,
      ip_protocol: "tcp",
      ip_ranges: [
        {
          cidr_ip: "0.0.0.0/0",
        },
      ],
      to_port: 80,
    },
   ],
  })

sgelb.create_tags({
  tags: [ # required
    {
      key: "Name",
      value: "Gc-Trial-ELB-SG",
    },
  ],
})
puts sgelb.id + '-->Security group for Load Balancer '

# Security group for Web Server

sgweb = ec2.create_security_group({
    group_name: 'Gc-Trial-WEB-SG-Group',
    description: 'Security group for Gc-Trial-WEB-SG',
    vpc_id: vpc.vpc_id
  })
sgweb.authorize_ingress({
  ip_permissions: [
    {
      from_port: 80,
      ip_protocol: "tcp",
      ip_ranges: [
        {
          cidr_ip: "0.0.0.0/0",
        },
      ],
      to_port: 80,
    },
   ],
  })

sgweb.create_tags({
  tags: [ # required
    {
      key: "Name",
      value: "Gc-Trial-WEB-SG",
    },
  ],
})

puts sgweb.id + '-->Security group for Webservers'

# RDS Security group.

sgdb = ec2.create_security_group({
    group_name: 'Gc-Trial-DB-SG-Group',
    description: 'Security group for Gc-Trial-DB-SG',
    vpc_id: vpc.vpc_id
  })
sgdb.authorize_ingress({
  ip_permissions: [
    {
      from_port: 3306,
      ip_protocol: "tcp",
      ip_ranges: [
        {
          cidr_ip: "0.0.0.0/0",
        },
      ],
      to_port: 3306,
    },
   ],
  })

sgdb.create_tags({
  tags: [ # required
    {
      key: "Name",
      value: "Gc-Trial-DB-SG",
    },
  ],
})

puts sgdb.id + '-->Security group for database'


# create ec2 instance.
instance1 = ec2.create_instances({
  image_id: 'ami-19f8df7c',
  min_count: 1,
  max_count: 1,
  key_name: 'gc-trial',
  security_group_ids: [sgweb.id],
  instance_type: 't2.micro',
  placement: {
    availability_zone: 'us-east-2a'
  },
  subnet_id: subnet1.id,
})
ec2.client.wait_until(:instance_running, {instance_ids: [instance1[0].id]})
#ec2.client.wait_until(:instance_running,)
#instance1.create_tags({ tags: [{ key: 'Name', value: 'GC-Trial-WEB-1' },],})
#p instance1.id
#puts instance1
puts instance1[0].id + '-->First ec2 instance under ELB'

instance2 = ec2.create_instances({
  image_id: 'ami-19f8df7c',
  min_count: 1,
  max_count: 1,
  key_name: 'gc-trial',
  security_group_ids: [sgweb.id],
  instance_type: 't2.micro',
  placement: {
    availability_zone: 'us-east-2b'
  },
  subnet_id: subnet2.id,
})
ec2.client.wait_until(:instance_running, {instance_ids: [instance2[0].id]})
#instance2.create_tags({ tags: [{ key: 'Name', value: 'GC-Trial-WEB-2' },],})
puts instance2[0].id + '-->Second ec2 instance under ELB'


#Create ELB

loadbalancer = elb.create_load_balancer({
  listeners: [
    {
      instance_port: 80,
      instance_protocol: "HTTP",
      load_balancer_port: 80,
      protocol: "HTTP",
    },
  ],
  load_balancer_name: "gc-trial",
  security_groups: [
    sgelb.id,
  ],
  subnets: [
   subnet1.id,
   subnet2.id,
  ],
})
puts loadbalancer 
# Health check for instance.
elbhealth = elb.configure_health_check({
  load_balancer_name: "gc-trial",
  health_check: {
  target: "HTTP:80/index.html",
  interval: 30 ,
  timeout: 5,
  unhealthy_threshold: 2,
  healthy_threshold: 10,
  },
})
puts elbhealth 

reginstance = elb.register_instances_with_load_balancer({
  load_balancer_name: "gc-trial", 
  instances: [ 
    {
      instance_id: instance1[0].id,
    },
    {
      instance_id: instance2[0].id,
    },
  ],
})
puts reginstance 

# Create RDS Mysql Subnet group
createsubnetgroup = rds.create_db_subnet_group({
  db_subnet_group_name: "GC-Trail-subnet-group", # required
  db_subnet_group_description: "GC-Trail-subnet-group", # required
  subnet_ids: [subnet3.id,subnet4.id], # required
  tags: [
    {
      key: "Name",
      value: "GC-Trail-Sbunet-Group",
    },
  ],
})

# Create Mysql  RDS 

createrds = rds.create_db_instance({
  db_instance_identifier: 'gc-trial',
  allocated_storage: 10,
  db_instance_class: "db.t2.micro", # required
  engine: "mysql", # required
  master_username: "admin",
  master_user_password: "adminc1234",
  db_subnet_group_name: "GC-Trail-subnet-group",
#  db_security_groups: ["sg-0201186b"],
  vpc_security_group_ids: [sgdb.id],
  backup_retention_period: 1,
  port: 3306,
  multi_az: false,
  tags: [
    {
      key: "Name",
      value: "gc-trial",
    },
  ],
  storage_type: "gp2",
})
